#usr/bin/R

#Problem 6: Hypothesis that people can delay their deaths until after important events
#Data: Deaths week before and after Passover; 922 before, 997 after, 1919 total
#Treat this as a binomial and test null hypothesis that theta = 1/2
theta.hat = 922/1919
se.hat = sqrt( theta.hat *(1-theta.hat)/1919)
W = (theta.hat - 1/2)/se.hat
2* pnorm(-abs(W))
#Also make a confidence interval for theta; choosing 95% just because
theta_CI = c(theta.hat - 1.96*se.hat, theta.hat + 1.96*se.hat)
theta_CI
#Note that a 91% CI should not contain theta0 = 1/2, the null, according to hypothesis test
theta_91CI = c(theta.hat - qnorm(.955)*se.hat, theta.hat + qnorm(.955)*se.hat)  
theta_91CI

#Problem 7: 1861, 10 essays appeared in New Orleans Daily Crescent signed Quintus Curtius Snodgrass
#Many suspected author was Mark Twain; to investigate we will consider proption of three letters found in an author's work
#From eight twain essays we have:
twain = c(.225, .262, .217, .240, .230, .229, .235, .217)
#From 10 Snodgrass essays we have:
snod = c(.209, .205, .196, .210, .202, .207, .224, .223, .220, .201)
#a) Perform a Wald test for equality of the means. Use the non-parametric plug in estimator.
#H0: delta = 0
twainmean = mean(twain)
snodmean = mean(snod)
delta.hat = twainmean - snodmean #plug-in estimator for difference in means
twainse.hat = sqrt(var(twain)/8 + var(snod)/10) #plug-in estimator for standard error
W.twain = abs(delta.hat/twainse.hat)
W.twain
pv.twain = 2*pnorm(-W.twain)
pv.twain
#The p-value for the Wald test is 0.00021, which constitutes strong evidence against the hypothesis that the Mark Twain and
#Snodgrass had equal proportions of three letter words in their essays. We can further interpret this as evidence against 
#the hypothesis that Mark Twain is the true identity of Snodgrass, but need to be more careful making this interpretation
#A 95% CI does not contain 0
twain_CI = c(delta.hat - 1.96*twainse.hat, delta.hat + 1.96*twainse.hat)
twain_CI
#b)Now use a permutation test to avoid the use of large sample methods
#18! is very large so more practical to simulate via monte carlo sampling
essays = c(twain, snod)
psim = rep(0, 10000)
#Does this test still have approximate validity if we allow repetition of permutations??
for (i in 1:10000){
	p = sample(essays)
	ptwain = p[1:8]
	psnod =p[-(1:8)]
	psim[i] = mean(ptwain) - mean(psnod)
}
psim[psim>delta.hat]
pv = length(psim[psim>delta.hat])/10000
pv
#We get a p-value of 9e-04, which is even smaller than the wald test, constituting strong evidence against the null that there is no
#difference in sample means. That is, under the null, we would expect only (approximately) a proportion <=9e-04 of permutations to show a difference
#as large as the one in this sample
#Here's a method to get rid of duplicate permutations
psim2 = matrix(rep(0, 10000 * 18), ncol = 10000, nrow = 18)
for (i in 1:10000){
	p = sample(essays)
	psim2[,i] = p
}
psim2 = unique(psim2, MARGIN = 2) #We care about permutations, not combinations so even if the exact set of essays get assigned to twain/snod
dim(psim2)		 	  #If they're in a different order it counts as a permutation
#Funnily enough this did not get rid of any columns
#I guess this makes sense; the chance of drawing the exact same permutation more than once in 10000 draws
#Could be modeled by a uniform multinomial distribution with (18!) components, p = 1/(18!)
#Not going to bother doing the math, but its very low, I'm sure

#Problem 10: Here are the number of elderly Jewish and Chinese women who died just before and after the Chinese Harvest Moon Festival:
harvest = data.frame(Chinese = c(55, 33, 70, 49), Jewish = c(141, 145, 139, 161), row.names = c(-2, -1, 1, 2))
harvest
#Compare the two mortality patterns. Kind of a vague instruction. Well, since this is the chapter on hypothesis testing
#Let's come up with a null hypothesis to test. p1 be the proportion of deaths among Chinese woman after the festival,
#p2 be the proportion of deaths among Jewish women after the festival. Let H0: p1 - p2 = 0
#We can then model both p1 and p2 as parameters from a binomial distribution. 
n = sum(harvest$Chinese)
m = sum(harvest$Jewish)
p1 = sum(harvest[3:4,]$Chinese)/n
p2 = sum(harvest[3:4,]$Jewish)/m
mle = p1 - p2 
se.harvest = sqrt( p1*(1-p1)/n + p2*(1-p2)/m )
#Applying the Wald test:
W.harvest = abs(mle/se.harvest)
W.harvest
p.harvest = 2*pnorm(-W.harvest)
p.harvest
#p-value of 0.12; so at best weak evidence against the hypothesis that the proportion of elderly Chinese vs. Jewish women who died two weeks after
#the harvest festival over the total number who died two weeks before and after did not differ.
#Of course, just at a passing glance the data suggests that the proportion of Chinese woman who died in the week immediately following 
#is higher than the rest. Let's assume a multinomial model for the data and perform a Chi-square test on the two groups.
H0 = rep(1/4, 4) #Null hypothesis: uniform multinomial distribution across the four weeks
chisq = function(data, p0){
	#Pearson's multinomial chisquare test; data is a vector of counts, p0 a vector of proportions under a putative null hypothesis
	#Note that the built-in chisq.test does the same thing, but this is good practice
	k = length(data)
	n = sum(data)
	nullvec = n*p0
	t = sum( ((data - nullvec)^2)/nullvec)
	p = pchisq(t, k-1, lower.tail = FALSE)
	return (p)
}
chisq(harvest$Chinese, H0) #p-value of 0.0035
chisq(harvest$Jewish, H0) #p-value of 0.56



#11)A randomized, double-blind experiment was conducted to assess the effectiveness of several drugs for reducing postoperative nausea.
#The data are as follows:
drugs = c('placebo', 'chlorpromazine', 'dimenhydrinate', 'pentobarbital_100_mg', 'pentobarbital_150_mg')
patients = c(80, 75, 85, 67, 85)
nauseainc = c(45, 26, 52, 35, 37)
df = data.frame(drugs, patients, nauseainc)
#a) Test each drug versus the placebo at the 5% level. Also, report the estimated odds-ratios. Summarize findings
#By testing each drug versus the placebo, I'm assuming the intent is apply the LRT on every drug with the null hypothesis that the proportion
#of nausea incidence is equal to that of the placebo. 
p0 = c(35/80, 45/80) #null p represented by the placebo proportions (no nausea, nausea)
c = qchisq(.05, 1, lower.tail = FALSE) #1 fixed parameter in data; 0 fixed under null, so 1 df
odds_ratios = rep(0, 4)
proportions = nauseainc/patients
for (i in 2:5){
	n = c(patients[i] - nauseainc[i], nauseainc[i])
	prop = c(1-proportions[i], proportions[i])
	ratio = 2* sum(n*log(prop/p0))
	odds_ratios[i-1] = ratio
}
odds_ratios = c(0, odds_ratios)
df = cbind(df, odds_ratios)
reject = odds_ratios > c 
df = cbind(df, reject)
#Assuming similar proportions of nausea incidence upon administration of placebo as the other drugs, the probability of obtaining levels of nausea
#incidence equal to those in patients administered chlorpomazine and 150 mg of pentobarbital is less than 5%. For dimenhydrinate and 100 mg of
#pentobarbital, the probability is greater than 5%
#b) Use the bonferroni and the FDR method to adjust for multiple testing
#need p-values explicitly
pval = pchisq(odds_ratios, 1, lower.tail = FALSE)
bon.threshhold = .05/4 
bonreject = pval < bon.threshhold
df = cbind(df, bonreject)
df = cbind(df, pval)

#Using the bonferonni method, we no longer reject the null for 150 mg of penobarbital. We still reject the null for chlorpromazine
BH.li = c(c(1:4) *.05/4, 'n/a')
df  <- df[order(pval),]
df = cbind(df, BH.li) 
df
#Using the benjamini-hochberg method, we retain rejection of the null for both chlorpromazine and 150 mg of pentobarbital



	


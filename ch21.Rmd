---
title: "Chapter 21: Smoothing using Orthogonal Functions"
output: 
 pdf_document: 
  extra_dependencies: ["amsmath", "bm"]
---

#### Exercise 1
*Prove theorem 21.5:*\
\
From Lemma 20.1, we can write the risk $R(J) = R(f, \hat f)$ with respect to squared
error loss as
\[ R(J) = \int b^2(x)dx + \int v(x)dx\]
where b and v represent bias and Variance terms of the estimator for fixed x:
\[b(x) = E[\hat f(x)] - f(x)\]
\[v(x) = Var(\hat f(x))\]
I will work with each term separately. For the bias, we have
\begin{align} E[\hat f(x)] &= \sum_{j=1}^{J} E[\hat{\beta}_j]\phi_j (x)\\
	                  &= \sum_{j=1}^{J} \beta_j \phi_j (x)
\end{align}
Thus, 
\[b(x) = \sum_{j=1}^J \beta_j \phi_j (x) - \sum_{j=1}^{\infty} \beta_j \phi_j (x)\]
Which gives us
\begin{align}
	\int b^2(x)dx &= \int (\sum_{j=J+1}^{\infty} \beta_j \phi_j (x))^2dx\\
                      &= \int (\sum_{j=J+1}^{\infty} \beta_j \phi_j (x))(\sum_{i=J+1}^{\infty} \beta_i \phi_i (x))dx\\
	              &= \sum_{j=J+1}^{\infty} {\beta_j}^2 \int \phi_j (x)^2 dx\\
                      &= \sum_{j=J+1}^{\infty} {\beta_j}^2
\end{align}
where steps (5) and (6) above follow from the fact that the $\phi_j$ form an orthogonal basis for $L_2[0,1]$.\
Now, since $Var(\hat{\beta_j}) = \frac{\sigma_j^2}{n}$, we have
\[Var(\hat f(x)) = Var(\sum_{j=1}^J \hat{\beta_j} \phi_j (x))\]
Which expands to
\begin{align}
Var(\hat f(x)) &= \sum_{j=1}^J {\phi_j}^2 (x) Var(\hat{\beta_j}) + 2\sum_{j=1}^{J} \sum_{i<j}Cov(\beta_j \phi_j (x), \beta_i \phi_i (x)) \\
               &= \sum_{j=1}^J {\phi_j}^2 (x) Var(\hat{\beta_j}) + 2\sum_{j=1}^{J} \sum_{i<j}Cov(\beta_j, \beta_i)\phi_j (x) \phi_i (x) \\
	       &= \sum_{j=1}^J \frac{\sigma^2_j}{n} {\phi_j}^2 (x) + 2\sum_{j=1}^{J} \sum_{i<j}Cov(\beta_j, \beta_i)\phi_j (x) \phi_i (x) \\
\end{align}
Integrating, we obtain
\begin{align}
\int v(x)dx & = \sum_{j=1}^J \frac{\sigma^2_j}{n}\int{\phi_j}^2 (x)dx + 2\sum_{j=1}^{J} \sum_{i<j}Cov(\beta_j, \beta_i)\int \phi_j (x) \phi_i (x) dx \\
         & =  \sum_{j=1}^J \frac{\sigma^2_j}{n}
\end{align}
Thus we have 
\[R(J) = \sum_{j=1}^J \frac{\sigma^2_j}{n} + \sum_{j=J+1}^{\infty} {\beta_j}^2\]
as desired.\

#### Exercise 2
*Prove Theorem 21.9* \
The regression estimator is 
\[ \hat r(x) = \sum_{j=1}^{J} \hat{\beta_j} \phi_j (x) \text{ where } \hat \beta_j = \frac{1}{n} \sum_{i=1}^{n} Y_i \phi_j (x_i).\]
Here, the variance of the estimator is
\begin{align}
Var(\hat r (x)) &= Var (\sum_{j=1}^{J} \hat{\beta_j} \phi_j (x)) \\
                &= \sum_{j=1}^{J} {\phi_j}^2 (x) Var(\hat{\beta_j}) + 2\sum_{j=1}^{J} \sum_{i<j}Cov(\beta_j, \beta_i)\phi_j (x) \phi_i (x) \\
                &\approx \sum_{j=1}^J \frac{\sigma^2}{n} {\phi_j}^2 (x) + 2\sum_{j=1}^{J} \sum_{i<j}Cov(\beta_j, \beta_i)\phi_j (x) \phi_i (x)
\end{align}
which gives, when integrated over x,
\begin{align}
	\int Var(\hat r(x)) &\approx  \sum_{j=1}^J \frac{\sigma^2}{n} \int{\phi_j}^2 (x)dx + 2\sum_{j=1}^{J} \sum_{i<j}Cov(\beta_j, \beta_i)\int \phi_j (x) \phi_i (x)dx\\
	                    &= \sum_{j=1}^J \frac{\sigma^2}{n} \\
			    &= \frac{J\sigma^2}{n}.
\end{align}

The bias is
\begin{align} 
	E[r(x) - \hat r(x)] &= r(x) - E[\sum_{j=1}^{J} \hat{\beta_j} \phi_j (x)]\\
	                    &= \sum_{j=1}^{\infty} {\beta_j} \phi_j (x))- \sum_{j=1}^{J} E[\hat{\beta_j]} \phi_j (x)\\
			    &approx \sum_{j=1}^{\infty} {\beta_j} \phi_j (x))- \sum_{j=1}^{J} \beta_j \phi_j (x)\\
			    &= \sum_{j=J+1}^{\infty} {\beta_j} \phi_j (x),
\end{align}
and integrating the bias squared over x gives
\begin{align}
\int E[r(x) - \hat r(x)]^2 dx&\approx \int ( \sum_{j=J+1}^{\infty} {\beta_j} \phi_j (x))^2 dx \\
                             &= \sum_{j=J+1}^{\infty} \sum_{i=J+1}^{\infty} \beta_j \beta_i \int \phi_j (x) \phi_i(x) dx \\
			     &= \sum_{j=J+1}^{\infty} {\beta_j}^2 \int {\phi_j}^2 (x) dx\\
			     &= \sum_{j=J+1}^{\infty} {\beta_j}^2. 
\end{align}
Together, the bias and variance give the (approximate) risk:
\[R(J) = \frac{J \sigma^2}{n} +  \sum_{j=J+1}^{\infty} \beta_j^2.\]

#### Exercise 3
*Let*
\[
 \psi_1 = (\frac{1}{\sqrt 3}, \frac{1}{\sqrt 3}, \frac{1}{\sqrt 3}), \ 
 \psi_2 =(\frac{1}{\sqrt 2}, -\frac{1}{\sqrt 2}, 0),\ 
 \psi_3 = (\frac {1}{\sqrt 6}, \frac {1}{\sqrt 6}, -\frac {2}{\sqrt 6}). 
\]
*Show that these vectors have norm 1 and are orthogonal.*
```{r}
psi1 = rep(1/sqrt(3), 3)
psi2 = c(1/sqrt(2), -1/sqrt(2), 0)
psi3  = c(1/sqrt(6), 1/sqrt(6), -2/sqrt(6))
norm_vec  <- function(x) sqrt(sum(x^2))
norm_vec(psi1)
norm_vec(psi2)
norm_vec(psi3)
psi1 %*% psi2 == 0
psi1 %*% psi3 == 0
psi2 %*% psi3 == 0
```

#### Exercise 4
*Prove Parseval's relation equation (21.6):*
\[ ||f||^2 \equiv \int f(x)^2 dx = \sum_{j=1}^{\infty} \beta_j^2 \equiv ||\beta||^2\]
Note this is with an orthonormal basis for $L_2(E)$, $\phi_j , \ j = 1, 2, \ldots$, so that $f(x)$ can be written:
\[f(x)  = \sum_{j=1}^{\infty} \beta_j \phi_j (x) \text{ where } \beta_j = \int_E f(x) \phi_j (x) dx. \]
With this, we have
\begin{align}
\int f(x)^2 dx &= \int  (\sum_{j=1}^{\infty} \beta_j \phi_j (x))^2 dx \\
               &= \sum_{j=1}^{\infty}\sum_{i=1}^{\infty} \beta_j \beta_i \int \phi_j (x) \phi_i (x) dx\\
	       &= \sum_{j=1}^{\infty} \beta_j^2 \int \phi_j (x)^2 dx\\
	       &= \sum_{j=1}^{\infty} \beta_j^2
\end{align}
as desired. Note that in (28) the terms in the double sum where $i \ne j$ are 0 because 
$\phi_i$ and $\phi_j$ are orthogonal.

#### Exercise 5
*Plot the first five Legendre polynomials. Verify numerically that they are orthonormal.*
The orthonormal version of the Legendre polynomials are defined in the text over $[-1,1]$ as:
\[\phi_j (x) = \sqrt{(2j+1)/2} P_j (x), \text{ where } 
P_j(x) = \frac{1}{2^j j!} \frac{d^j}{dx^j}(x^2-1)^j.
\]
Which may be constructed using the recursive relation
\[P_{j+1}(X) = \frac{(2j+1)xP_j(x)- jP_{j-1}(x)}{j+1}\]
```{r}
leg = function(x, j){
#function implementing recursive calculation of legendre polynomial
	if (j == 0){
		return(rep(1,length(x)))
		}
	if (j == 1){
		return(x)
		}
	P = ((2*(j-1)+1) * x * leg(x, j-1) - (j-1) * leg(x, j-2))/j
	return (P)
}
leg_ortho = function(x, j){
#normalizing the legendre polynomials
	b = sqrt((2*j+1)/2)
	return(b*leg(x,j))
}
#graphs
curve(leg_ortho(x, 4), from = -1, to = 1, col = 5)
curve(leg_ortho(x, 0), from = -1, to = 1, col = 1, add = TRUE)
curve(leg_ortho(x, 1), from = -1, to = 1, col = 2, add = TRUE)
curve(leg_ortho(x, 2), from = -1, to = 1, col = 3, add = TRUE)
curve(leg_ortho(x, 3), from = -1, to = 1, col = 4, add = TRUE)
#checking inner product of two normalized legendre polynomials
orthocheck = function(i, j){
	int = integrate(function(x) leg_ortho(x, i) * leg_ortho(x, j), -1, 1)$value
	return(int)
}
outer(0:4, 0:4, Vectorize(orthocheck))
```
The diagonal entries of the final matrix show that each Legendre polynomial is normalized, while
the off-diagonal entries show the first 5 are orthogonal to each other.

#### Exercise 6
*Expand the following functions in the cosine basis on $[0,1]$. For (a) and (b), find the coefficients
$\beta_j$ analytically. For (c) and (d), find the coefficients numerically. Then plot the partial sums for increasing values of n.*\

(a) $f(x) = \sqrt{2} cos(3\pi x)$
Since $\phi_j (x) = \sqrt2 cos(j\pi x)$, the product-sum identity for cosine gives:
\[ f(x) \phi_j (x) = 2 cos (3\pi x) cos(j\pi x) = cos( (3-j) \pi x) + cos((3+j)\pi x)\]
so that
\begin{align}
\beta_j = \int_0^1 f(x) \phi_j (x) &= \int_0^1 cos((3-j)\pi x) + cos((3+j)\pi x)\\
                                   &=\frac{1}{(3-j)\pi} sin((3-j)\pi ) 
				   + \frac{1}{(3+j)\pi} sin((3+j\pi )
\end{align}
which is equal to 0. This seems absurd, but note that the above does not apply for $j=3$, 
since then step 31 becomes
\[\int_0^1 cos((3-3)\pi x) + cos((3+j)\pi x) = \int_0^1 1 + cos((3+j)\pi x) = 1,\]
which clearly makes sense since $\phi_3 (x) = f(x)$. Plotting the partial sums is just plotting
the 0 function and the function itself.
```{r}
curve(sqrt(2) * cos(3*pi*x), 0, 1)
curve(0*x, 0, 1, add = TRUE)
```
(b) $f(x) = sin(\pi x)$
First, since $\phi_0 = 1$ for the cosine basis, we have
\[\beta_0 = \int_0^1 sin(\pi x) dx = \frac{2}{\pi}.\]
For $j\neq 0$, the product-sum formula for sin and cosine gives
\[f(x) \phi_j (x) = sin(\pi x) \sqrt 2 cos(j\pi x) = \frac{\sqrt 2}{2}(sin(1+j)\pi x 
					      +sin(1-j)\pi x)\]
To obtain $\beta_j$, we integrate.
\[\beta_j = \int_0^1 \frac{\sqrt 2}{2}(sin(1+j)\pi x +sin(1-j)\pi x),\]
When $j=1$, we have
\[\beta_j = -\frac{\sqrt 2}{4\pi}cos(2\pi x)\Bigr|_0^1 = 0.\]
Otherwise, we have
\[\beta_j = 
 -\frac{\sqrt 2}{2}\Bigl(\frac{1}{\pi+\pi j}cos(1+j)\pi x
  +\frac{1}{\pi-\pi j}cos(1-j)\pi x\Bigr) \Bigr|_0^1\]
which equals 0 when j is odd, $j \neq 1$, and
\[\beta_j = \frac{2\sqrt2}{\pi-\pi j^2}\]
when j is even.
```{r}
sinjcoef = function(j){
#compute beta coefficients
	if (j == 0){
		return(2/pi)
		}
	if (j %%2 == 1){
		return(0)
		}
	beta = sqrt(2)*2/(pi-pi*j^2)
	return (beta) 
}
sumcos = function(x, beta, J){
#gives j'th partial sum of cosine basis approximation to function at x, 
#given a function beta to calculate the coefficients
	sum = beta(0) 
	for (j in 1:J){
		sum = sum + beta(j)*sqrt(2)*cos(j*pi*x)
		}
	return(sum)
}
Jvec = seq(from =0, to = 10, length.out = 6)
#J ranging from 0 to 10, since odd terms don't affect sum
curve(sin(pi*x), from = 0, to = 1, col = 1, ylim = c(0, 1.1))
for (i in Jvec){
	curve(sumcos(x, sinjcoef, i), from = 0, to = 1, col = i/2+2, add = TRUE)
}
legend(0.4, 0.7, legend = c("f", paste(rep("J =", 6),  as.character(Jvec))), 
	lty = rep(1, 7), col = c(1:7))
```
(c)$f(x) = \sum_{j=1}^{11}h_jK(x-t_j)$ where $K(t) = (1+sign(t))/2$, \
   $(t_j) = (.1, .13, .15, .23, .25, .40, .44, .65, .76, .78, .81)$,\
   $(h_j) = (4, -5, 3, -4, 5,, -4.2, 2.1, 4.3, -3.1, 2.1, -4.2)$
```{r, out.height = "25%"}
fc = function(x){
	t = c(.1, .13, .15, .23, .25, .40, .44, .65, .76, .78, .81)
	h = c(4, -5, 3, -4, 5, -4.2, 2.1, 4.3, -3.1, 2.1, -4.2)
	f = sapply(x, function(x) sum(h*(1+sign(x-t))/2))
	return (f)
}
fcbeta = function(j){
	if (j==0){
	#I fixed the number of subdivisions since the integrate function was hitting a max
	#and throwing an error. Probably due to adaptive subdivision choice + the "spikiness"
	#of the distribution (see plot below)
		beta = integrate(function(x) fc(x), 0, 1, subdivisions = 10000)
		return(beta$value)
	}
	beta = integrate(function(x) fc(x)*sqrt(2)*cos(pi*j*x), 0, 1, subdivisions = 10000)
	return (beta$value)
}
cJvec = (0:5)
for (i in cJvec){
	curve(fc(x), from = 0, to = 1, 	main = paste("J=", as.character(i*10)), ylim = c(-2, 6))
	curve(sumcos(x, fcbeta, i*10), from = 0, to=1, col = 2, add = TRUE)
}
```
In the above plots, the black curve is the function f and the red curves 
are the partial sum approximations.
```{r, echo = FALSE}
#could optimize above code by calculating beta coefficients once and inputting into the curve loop,
#rather than calculating repeatedly thru sumcos function (e.g. currently 1-9th coefficients are
#evaluated 5 times each, each time doing expensive numerical integration
```
\
(d) $f(x) = \sqrt{x(1-x)} sin\Bigl(\frac{2.1\pi}{x+.05}\Bigr)$
```{r, out.height = "25%"}
fd = function(x) sqrt(x*(1-x)) * sin(2.1*pi/(x+.05))
dBeta = rep(0, 50)
dBeta[1] = integrate(function(x) fd(x), 0, 1)$value
for (j in 1:50){
	dBeta[j+1] = integrate(function(x) fd(x) * sqrt(2)*cos(pi*j*x), 0, 1)$value
}
phiJ = function(x, J) sqrt(2) * cos((0:J)*pi*x)
Jsum = Vectorize(function(x, Beta) sum(phiJ(x, length(Beta)-1) * Beta), 'x')
Jsum(c(0,0.5, 1), dBeta[0:10])
for (i in cJvec){
	curve(fd(x), from = 0, to = 1, main = paste("J =", as.character(i*5)), 
		ylim=c(-0.5, 0.6),n=1000) #n is number of points plotted;needs to be high due to
					  #high frequency of function in regions where x small
	curve(Jsum(x, dBeta[1:(i*5+1)]) ,col = 2, add = TRUE)
}
```
Its clear that good approximation for the high-frequency regions where x is small requires
higher order partial sums.

#### Exercise 7
*Consider the glass fragments data from the book's website. Let Y be refractive index and let
X be aluminum content.*
(a)*Do a nonparametric regression to fit the model $Y = f(x) + \epsilon$ using the cosine basis.*
```{r}
glass = read.table("glass.dat")
glass = glass[order(glass$Al),]
X = glass$Al
Y = glass$RI
phicos = function(x, j) if (j == 0) 1 else sqrt(2)*cos(j*pi*x)
ortho_regression = function(X, Y, phi){
	scaleto01 = function(x) (x-min(X))/(max(X) - min(X))
	n = length(X)	
	beta = rep(0, n+1)
	for (j in 0:n){
		beta[j+1] = 1/n*sum(Y*phi(scaleto01(X),j))
		}
	#Estimating risk
	k = round(n/4)
	v = n/k * sum(beta[(n+2-k):(n+1)]^2)
	biasVec = sapply(beta^2 - v/n, max, 0)
	risk = function(J) (J+1)*v/n +  sum(biasVec[(J+2):(n+1)])
	#Choosing J to minimize risk and defining the regression function
	J = which.min(sapply(0:n, risk))
	r_hat =  function(x) {
		xx = scaleto01(x)
		yhat = rep(0, length(x)) 
		for (j in 1:J+1){
			yhat = yhat + beta[j] * phi(xx, j-1)
			}
		return(yhat)
	}

	Jrisk = risk(J)
	

	#constructing confidence band
	CI = function(x, alpha){
		xx = scaleto01(x)
		a = rep(0, length(x))
		for(j in 0:J){
			a = a + phi(xx, j)^2
		}
		c = sqrt(a*v*qchisq(1-alpha, J)/n)
		interval = cbind(r_hat(x), r_hat(x)) + cbind(-c,c)
		return (interval)
		}
	output = list(fit = r_hat,J = J, risk = Jrisk,CI = CI, beta = beta[1:J+1])
	return(output)
}
glassfit = ortho_regression(X,Y,phicos)

plot(X,Y,xlab = "Aluminum Content", ylab = "Refractive Index") 
curve(glassfit$fit(x), 0, max(X), col = 2, add = TRUE)
xs = seq(from = 0, to = max(X), length.out = 5000)
polygon(c(xs, rev(xs)), c(glassfit$CI(xs,alpha=0.05)[,1], 
	rev(glassfit$CI(xs, alpha=0.05)[,2])), col = rgb(0.5, 0.5,0.5, 0.2) , lty = 'dotted')

```
(b)*Use the wavelet method to estimate f.*  
Process for Haar Wavelet Regression:  
1. Let $J = log_2(n)$ and define
\[\hat{\alpha}= \frac{1}{n} \sum_i \phi_k(x_i)Y_i 
\text{  and  }
D_{j,k} = \frac{1}{n}\psi_{j,k}(x_i)Y_i\]
for $0 \le j \le J-1$.
2. Estimate $\sigma$ by
\[ \hat{\sigma} = \sqrt n \times \frac{median(|D_{J-1,k}|: k=0,\dots, 2^{J-1} -1)}{0.6745}.\]
3. Apply universal threshholding:
\[\hat \beta_{j,k} =\begin{cases}
		D_{j,k} & if |D_{j,k}| > \hat \sigma \sqrt{\frac{2logn}{n}}\\
		0       & otherwise
		\end{cases}
		\]
4. Set $\hat f(x) = \hat \alpha \phi(x) +
		   \sum_{j=0}^{J-1}\sum_{k=0}^{2^j-1} \hat \beta_{j,k} \psi_{j,k}(x).$

In practice, $D_{j,k}$ are calculated using the Discrete Wavelet Transform:
```{r}
DWT = function(Y){
	n = length(Y)
	J = floor(log2(n))
	D = vector('list', J)
	temp = Y/sqrt(n)
	for (j in (J):1){
		m = 2^(j-1)
		I = 1:m
		D[[j]]  <- (temp[2*I] -temp[(2*I)-1])/sqrt(2)
		temp  <- (temp[2*I] +temp[(2*I)-1])/sqrt(2)
		}
	return(D)
}
```
The above algorithm is derived from the appendix of the book. However, it clearly does not
depend on X, but the given formula for $D{j,k}$ does. I could be missing something, but
I don't know enough, so I just won't use the transform.
```{r}
psiHaar = function(x, j, k){
	mother = function(x) if (0<=x & x<=1) if (x<=1/2) -1 else 1 else 0
	output = 2^(j/2) * sapply(2^j*x - k, mother)
	return (output)
}
regressionHaar = function(X, Y){
	scaleto01 = function(x) (x-min(X))/(max(X) - min(X))
	#Scaling to [0,1]
	scaledX = scaleto01(X)
	n = length(Y)

	alpha_hat = sum(Y)/n
	J = ceiling(log2(n))

	#Estimating coefficients
	D = list(vector, J)
	betaHat = D
	for (j in 1:J){
		D[[j]] = rep(0, 2^(j-1))
		betaHat[[j]] = D[[j]]
		for (k in 1:(2^(j-1))){
			D[[j]][k] = sum(psiHaar(scaledX, j-1, k-1)*Y)/n
			}}
	
	#Applying Universal threshholding
	sigHat = sqrt(n) * median(abs(D[[J]] ))/0.6745
	threshhold = sigHat * sqrt(2*log(n)/n) 
	for (j in 1:J){
		above = which(abs(D[[j]]) > threshhold)
		betaHat[[j]][above] = D[[j]][above]
		}

	#Defining the regression function
	fHat = function(x){
		xx = scaleto01(x)
		est = alpha_hat*xx
		for (j in 1:J){
			for (k in 1:(2^(j-1))){
				est = est + psiHaar(xx, j-1, k-1)*betaHat[[j]][k]
				}
			}
		return(est)
		}
	return (fHat)
}
glassHaar = regressionHaar(X, Y)

plot(X, Y, xlab = "Aluminum Content", ylab = "Refractive Index", ylim = c(-15, 25))
curve(Vectorize(glassHaar)(x), add = TRUE, n = 1000)
```

#### Exercise 8
*Show that the Haar wavelets are orthonormal.*
The Haar father wavelet is defined by
\[ \phi(x) = \begin{cases}
	1 & \text{if } 0\le x<1\\
	0 & otherwise
	\end{cases}\]
The mother Haar wavelet is defined by:
\[\psi(x) = \begin{cases}
	-1 & \text{if } 0\le x\le \frac{1}{2}\\
	 1 & \text{if } \frac{1}{2} < x \le 1.
	 \end{cases}\]
For any integers $j$ and $k$ define
\[\psi_{j,k}(x) = 2^{j/2} \psi(2^j x - k).\]
First, I show normality. This is immediately clear for the father and mother wavelet functions.
For the other wavelet functions, note we can rewrite them as:
\begin{equation} \psi_{j,k}(x) = \begin{cases}
		-2^{j/2} & \text{if }  \frac{k}{2^j}\le x \le \frac{1/2+k}{2^j}\\
		 2^{j/2} & \text{if }  \frac{1/2+k}{2^j}< x \le \frac{1+k}{2^j}
		 \end{cases}
\end{equation}
It follows that
\[\int_0^1 \psi_{j,k}^2(x) dx = \int_{\frac{k}{2^j}}^{\frac{1+k}{2^j}}2^j dx =\frac{2^j}{2^j} = 1.\]
Now, I show orthogonality. It is clear from inspection that $\int_0^1\psi_{j,k} dx = 0$, which
implies $\phi(x)$ is orthogonal to the other wavelets (the mother wavelet is just the case
of $j=k=0$). Suppose we have two functions $\psi_{j,k}$ and $\psi_{j,m}$, $k \ne m$. Then
by equation (33), the set over which both are nonzero is either empty or a single point, and thus
\[\int_0^1 \psi_{j,k}(x)\psi_{i,k}(x)dx = 0.\]
Take two functions $\psi_{j,k}$ and $\psi_{i,m}$, and suppose without loss of generality
that $i<j$. By similar logic as before, if their supports 
overlap at most for a single point, then they are orthogonal. Moreover, 
for any n, if $\frac{k}{2^j}<\frac{n/2}{2^i}<\frac{k+1}{2^j}$, then
\[ 0<\frac{2^{j-i-1}nm-k}{2^{j}} <\frac{1}{2^{j}},\]
and since $i<j$, $0\le j-i-1$ and the numerator in the middle must be a positive integer less than
1, which is a contradiction. This shows that if the supports overlap at 
more than a single point, the support of $\psi_{j,k}$ is a subset of either the top half or the
bottom half of the support of $\psi_{i,m}$. In the former case, we have
\[\int_0^1 \psi_{j,k}(x)\psi_{i,m}(x)dx = 
	\int_{\frac{k}{2^j}}^\frac{k+1/2}{2^j}(-2^j)(-2^i)dx +
	\int_{\frac{k+1/2}{2^j}}^\frac{k+1}{2^j}(2^j)(-2^i)dx = 0.
\]
The latter case is similar. 

#### Exercise 9
*Consider again the doppler signal:
\[f(x) = \sqrt{x(1-x)} sin\Bigl(\frac{2.1\pi}{x+.05}\Bigr)\]
Let $n =1024,$ $\sigma = 0.1,$ and let $(x_1,\dots,x_n) = (1/n,\dots,1)$. Generate data
\[Y_i = f(x_i) + \sigma \epsilon_i\]
where $\epsilon_i ~ N(0,1)$.
(a) Fit the curve using the cosine basis method. Plot the function estimate and confidence
band for $J = 10, 20, \dots, 100$.*
First adapting my regression function to use a given J rather than the J which minimizes risk.
```{r}
ortho_regressionJ = function(X, Y, phi, J){
	scaleto01 = function(x) (x-min(X))/(max(X) - min(X))
	n = length(X)	
	beta = rep(0, J+1)
	for (j in 0:n){
		beta[j+1] = 1/n*sum(Y*phi(scaleto01(X),j))
		}
	#Estimating risk
	k = round(n/4)
	v = n/k * sum(beta[(n+2-k):(n+1)]^2)
	biasVec = sapply(beta^2 - v/n, max, 0)
	risk = function(J) (J+1)*v/n +  sum(biasVec[(J+2):(n+1)])

	r_hat =  function(x) {
		xx = scaleto01(x)
		yhat = rep(0, length(x)) 
		for (j in 1:J+1){
			yhat = yhat + beta[j] * phi(xx, j-1)
			}
		return(yhat)
	}

	Jrisk = risk(J)
	

	#constructing confidence band
	CI = function(x, alpha){
		xx = scaleto01(x)
		a = rep(0, length(x))
		for(j in 0:J){
			a = a + phi(xx, j)^2
		}
		c = sqrt(a*v*qchisq(1-alpha, J)/n)
		interval = cbind(r_hat(x), r_hat(x)) + cbind(-c,c)
		return (interval)
		}
	output = list(fit = r_hat,J = J, risk = Jrisk,CI = CI, beta = beta[1:J+1])
	return(output)
}
```
Now data generation and fitting
```{r}
X = seq(from = 1/1024, to = 1,length.out = 1024)
Y = fd(X) + 0.1*rnorm(1000)
optimalDopplerfit = ortho_regression(X,Y,phicos)
dopplerfits = vector(mode = 'list', 10)
for (i in 1:10){
	dopplerfits[[i]] = ortho_regressionJ(X,Y,phicos,i*10)
}
```
Plotting
```{r}

plot(X,Y,xlab = "x", ylab = "y", 
	main = "Risk-optimized fit (J=189)", pch = 20, col = rgb(1, 0, 0, 0.2),
	ylim = c(-1.15,1.15))
curve(optimalDopplerfit$fit(x), 0, 1, col = 1, add = TRUE)
xs = seq(from = 0, to = 1, length.out = 5000)
polygon(c(xs, rev(xs)), c(optimalDopplerfit$CI(xs,alpha=0.05)[,1], 
	rev(optimalDopplerfit$CI(xs, alpha=0.05)[,2])), 
	col = rgb(0.5, 0.5,0.5, 0.2) , lty = 'dotted')
```
```{r,out.height = "25%"}
for (i in 1:10){
	plot(X,Y,xlab = "x", ylab = "y", 
		main = paste("J =", as.character(i*10)), pch = 20, col = rgb(1, 0, 0, 0.2),
		ylim = c(-1.15,1.15))
	curve(dopplerfits[[i]]$fit(x), 0, max(X), col = 1, add = TRUE)
	polygon(c(xs, rev(xs)), c(dopplerfits[[i]]$CI(xs,alpha=0.05)[,1], 
		rev(dopplerfits[[i]]$CI(xs, alpha=0.05)[,2])), 
		col = rgb(0.5, 0.5,0.5, 0.2) , lty = 'dotted')
}
```
We see low-resolution (low J) regression with the cosine basis does not capture the high resolution
structure of the doppler function at low X. A higher resolution (higher J) can better capture
the variation at low x, but clearly overfits to the noise for higher values of x.

(b) Use Haar wavelets to fit the curve
```{r}
dopplerHaar = regressionHaar(X,Y)
plot(X,Y,xlab='x', ylab='y', 
	main = "Haar Wavelet Regression for the Doppler function", 
	pch = 20, col=rgb(1,0,0,0.2)) 
curve(dopplerHaar(x), 0, 1, col = 1, n = 1000, add = TRUE)
```
#### Exercise 10
*(Haar density Estimation.) Let $X_1,\dots,X_n ~ f$ for some density f on [0,1]. Let's
consider constructing a wavelet histogram. Let $\phi$ and $\psi$ be the Haar father
and mother wavelet. Write*
\[f(x)\approx \phi(x) + \sum_{j=0}^{J-1}\sum_{k=0}^{2^j-1}\beta_{j,k} \psi_{j,k}(x)\]
*where $J \approx log_2(n).$ Let*
\[\hat \beta_{j,k} = \frac{1}{n} \sum_{i=1}^n \psi_{j,k}(X_i).\]
(a)*Show that $\hat\beta_{j,k}$ is an unbiased estimate of $\beta_{j,k}$.*  
We have
\begin{align}
 E[\hat\beta_{j,k}]& = E[\frac{1}{n} \sum_{i=1}^{n}\psi_{j,k} (X_i)] \\
 		   & = \frac{1}{n} \sum_{i=1}^{n}E[\psi_{j,k} (X_i)] \\
		   & = \frac{1}{n} \sum_{i=1}^n\int_0^1 f(x)\psi_{j,k}(x)dx\\
		   & = \frac{1}{n} \sum_{i=1}^n \beta_{j,k}\\
		   & = \beta_{j,k}
\end{align}
(b) Define the Haar histogram
\[\hat f(x) = \phi(x) + \sum_{j=0}^B\sum_{k=0}^{2^j-1}\hat \beta_{j,k}\phi_{j,k}(x)\]
for $0\le B\le J-1$.  
Note that if f is a probability distribution, the coefficient for $\phi$ will always be 1
since $\phi$ is the identity on $[0,1]$, and thus $\int_0^1 f(x)\phi(x) = \int_0^1f(x) = 1$.
(c) Find an approximate expression for the MSE as a function of B.  
I start with the bias term. First, we write
\[E[\hat f(x)] \approx \phi(x) + \sum_{j=0}^{B}\sum_{k=0}^{2^j-1}E[\hat \beta_{j,k}]\psi_{j,k}(x)
	      = \phi(x) + \sum_{j=0}^{B}\sum_{k=0}^{2^j-1}\beta_{j,k}\psi_{j,k}(x)\]
which in turn gives us, using the approximation for f given in the problem statement
\[E[f(x) - \hat f(x)] = \sum_{j=B+1}^{J-1}\sum_{k=0}^{2^j-1} \beta_{j,k}\psi_{j,k}(x).\]
Integrating the square of the bias over x, we have
\begin{align}
	\int E[f(x) - \hat f(x)]^2dx 
	&\approx \int \Bigl(\sum_{j=B+1}^{J-1}\sum_{k=0}^{2^j-1} \beta_{j,k}\psi_{j,k}(x)\Bigr)^2dx\\
	&= \int \Bigl(\sum_{j=B+1}^{J-1}\sum_{k=0}^{2^j-1} \beta_{j,k}\psi_{j,k}(x)\Bigr)
	\Bigl(\sum_{i=B+1}^{J-1}\sum_{m=0}^{2^i-1} \beta_{i,m}\psi_{i,m}(x)\Bigr)dx\\
	&=\sum_{j=B+1}^{J-1}\sum_{k=0}^{2^j-1} \sum_{i=B+1}^{J-1}\sum_{m=0}^{2^i-1} 
	\beta_{j,k}\beta_{i,m}\int \psi_{j,k}(x)\psi_{i,m}(x)dx\\
	&=\sum_{j=B+1}^{J-1}\sum_{k=0}^{2^j-1} \beta_{j,k}^2
\end{align}
For the variance term, first write
\[\sigma_{k,j}^2 = Var(\psi_{k,j}(X_i)) = \int(\phi_{j,k}-\beta_{j,k})^2 f(x)dx.\]
Assuming independence of the $X_i$, we can write
\[Var(\hat \beta_{j,k}) = Var(\frac{1}{n}\sum_{i=1}^{n}\psi_{j,k}(X_i))
		    = \frac{1}{n^2}\sum_{i=1}^{n}Var(\psi_{j,k}(X_i)) = \frac{\sigma_{j,k}^2}{n}.\]
Thus, for fixed x
\begin{align}
	Var(\hat f(x))&= Var(\phi (x)+\sum_{j=0}^{B}\sum_{k=0}^{2^j-1}\hat \beta_{j,k} \psi_{j,k}(x))\\
		      &= \sum_{j=0}^{B}\sum_{k=0}^{2^j-1}Var(\hat \beta_{j,k}) \psi_{j,k}(x)
		         +\sum_{j=0}^{B}\sum_{k=0}^{2^j-1}\sum_{i<j}\sum_{m=0}^{2^i-1}
			  Cov(\hat \beta_{j,k}, \hat \beta_{i,m})\psi_{j,k}(x)\psi_{i,m}(x).
\end{align}
We then integrate the variance over x:
\begin{align}
	\int Var(\hat f(x) &= \sum_{j=0}^{B}\sum_{k=0}^{2^j-1}Var(\hat \beta_{j,k}) \int \psi_{j,k}(x)dx
		             +\sum_{j=0}^{B}\sum_{k=0}^{2^j-1}\sum_{i<j}\sum_{m=0}^{2^i-1}
			     Cov(\hat \beta_{j,k}, \hat \beta_{i,m})\int \psi_{j,k}(x)\psi_{i,m}(x)dx\\
			   &= \sum_{j=0}^{B}\sum_{k=0}^{2^j-1}\frac{\sigma_{j,k}^2}{n}
\end{align}
Combining the derived bias and variance terms gives us the following approximation for the mean
squared error risk of the Haar histogram estimator as a function of B:
\[R(B) = \sum_{j=0}^{B}\sum_{k=0}^{2^j-1}\frac{\sigma_{j,k}^2}{n}
      + \sum_{j=B+1}^{J-1}\sum_{k=0}^{2^j-1} \beta_{j,k}^2\]
We can estimate the risk by adapting the risk estimator given in the book for general orthogonal
density estimation.
\[\hat R(B) = \sum_{j=0}^B\sum_{k=0}^{2^j-1} \frac{\hat \sigma^2_j}{n} +
	\sum_{j=B+1}^{J-1}\sum_{k=0}^{2^j-1}\hat \Bigl(\beta_j^2 - \frac{\hat \sigma_j^2}{n}\Bigl)_+,\]

The justification that $\hat \sigma_j^2$ is an approximately unbiased estimate of $\sigma_j^2$
and $\hat \beta_j^2 - \hat \sigma_j^2$ is an approximately unbiased estimate of $\beta_j^2$ still holds.

(d) *Generate n =1,000 observations from a Beta (15,4) density. Estimate the density using the Haar
histogram. Use leave-one-out cross validation to choose B.*  
Here I will use the leave-one-out CV risk estimator:
\[\hat R(B) = \int\bigl(\hat f_B(x)\bigr)^2dx 
	- \frac{2}{n}\sum_{i=1}^n \hat f_{-i} (X_i).\]

```{r, eval = FALSE}
histHaar = function(X, B){
	n = length(X)

	#Estimating coefficients
	betaHat = list(vector, B)
	for (j in 1:B){
		betaHat[[j]] = rep(0, 2^(j-1))
		for (k in 1:(2^(j-1))){
			betaHat[[j]][k] = sum(psiHaar(X, j-1, k-1))/n
			}}
	

	#Defining the Density estimation function
	fHat = function(x){
		est = 1
		for (j in 1:B){
			for (k in 1:(2^(j-1))){
				est = est + psiHaar(x, j-1, k-1)*betaHat[[j]][k]
				}
			}
		return(est)
		}
	#Leave-one-out cross-validation
	return (fHat)
}

leave1OutRisk = function(X, B){
	estimator = function(x,b) histHaar(x, b)
	n = length(X)
	fHat = estimator(X, B)
	fnTerm = integrate(function(x) fHat(x)^2, 0, 1, subdivisions = 10000)$value
	leave1out = 0
	for (i in 1:n){
		xx = X[-i]
		leave1out = leave1out + estimator(xx, B)(X[i])
		}
	return (fnTerm - (2/n)*leave1out)
}
X = rbeta(1000, 15, 4)
J = ceiling(log2(1000))
risk = rep(0,8) 
for (B in 1:8){
	risk[B] = leave1OutRisk(X, B)
}
bestB = which.min(risk)
bestB
fHat = histHaar(X, bestB)
curve(fHat(x), from = 0, to = 1, n = 1000,main = paste("B =", as.character(bestB)))
	add = TRUE, col = 2, lty = 2,from=0,to=1, n=1000)
curve(dbeta(x,15,4), col =2, lty = 2, add = TRUE)
legend(x=0.1, y=4, legend=c("B = 5 wavelet estimate", "Beta(15, 4)"), col = c(1,2), lty = c(1,2))


```


#### Exercise 11
*In this question we will explore the motivation for equation (21.3). Let
$X_1, \dots,X_n ~ N(0, \sigma^2)$. Let*
\[\hat \sigma = \sqrt n \times \frac{median(|X_1|, \dots, |X_n|)}{0.6745}.\]
(a) Show that $E[\hat \sigma] = \sigma$  
To do this, we need to derive the distribution of the median, and to do that,
we need to derive the distribution of $Y_i=|X_i|$. We have
\begin{align}
	F_y(Y) =P(|X_i|\le x) &= P(-x \le X_i \le x)\\
	                      &= F(x) - F(-x)\\
		              &= 2F(x) - 1
\end{align}
Differentiating gives us the pdf of $Y_i = |X_i|$:
\[f_Y(y) = 2f_x(x) = \frac{2}{\sigma \sqrt{2\pi}}e^-{\frac{x^2}{2\sigma^2}} \text{ for } 0\le x <\inf\]



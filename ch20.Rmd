---
title: "Chapter 20: Nonparametric Curve Estimation Exercises"
output: pdf_document
---

Loading packages
```{r, message = FALSE}
library(ggplot2)
library(functional)
library(dplyr)
```
## Exercise 2 

Get the data on fragments of glass collected in forensic work from book website
Estimate density of the first variable(refractive index) using a histogram and a KDE.
Use cross-validation to choose the amount of smoothing.
Experiment with different binwidths and bandwidths.
Comment on the similarities and differences. Construct 95% confidence bands for estimators

Let's make functions to make bin counts for histograms and to calculate cross-validation scores

```{r}
histogram = function(n, data){
	#returns height of bins for given bin number n, for data vector
	if (n == 1){
		return (length(data))
	}
	m = max(data)
	l = min(data)
	h = (m - l)/n
	binbounds = rep(0, n+1)
	for (i in 1:(n+1)){
		binbounds[i] = l + (i-1)*h	
	}	
	binbounds[n+1] = m
	p = rep(0, n)
	data = sort(data)
	for (j in 1:n){
		min = binbounds[j]
		max = binbounds[j + 1]
		if (j == n){
			p[j] = length(data[min < data & data <= max])
		}
		else{
			p[j] = length(data[min <= data & data < max])

		}
	}
	return(p)
}
crossVal = function(histo, data){
	n = length(refractive)
	p = histo/n
	h = (max(data) - min(data))/length(histo)
	J = 2/((n-1) * h) - (n+1)/((n-1)*h) * sum(p^2)
	return (J)
}
```

Now let's find the bin number from 1:200 that minimizes the cross-validation score
```{r}
glass = read.table("glass.dat")
refractive = glass$RI
widths = c(1:200)

histograms = lapply(widths, histogram, data = refractive) 
sapply(histograms, sum)#Checking to make sure all are 100

risk = sapply(histograms, crossVal, data = refractive)
n = which(risk == min(risk))
```
We find the minimum at n = 34 bins:
```{r, include = FALSE}
plot(x = 1:200, y = risk, type = "lines", xlab = "Number of Bins", ylab = "Estimated Risk")
```
Now let's look at the histogram using 34 bins
```{r}
hist(refractive, breaks = n) 
```
Now I'll write a function to make a 1-alpha confidence band for a histogram
```{r}
cBand = function(x, c) c(max(c(sqrt(x)-c, 0)), (sqrt(x) + c))^2
hist_CI = function(histo, alpha = 0.05){
	#Takes as input a vector representing a histogram in the form of bin counts
	n = sum(histo)
	m = length(histo)
	c = qnorm(1-alpha/(2*m))/2 * sqrt(m/n)
	CI = sapply(histo, cBand, c = c) 
	rownames(CI) = c("lower", "upper")
	return(CI)
}
counts34 = histograms[[34]]
CI = hist_CI(counts34)
```
Now to plot it: I need to do some manual calculation to get it looking nice.
I need to manually set the bin bounds because by default R rounds the lowest
and highest bin bounds to an integer, which messes up the x positions of the CI bounds
using bincenters.

```{r}
ma = max(refractive)
mi = min(refractive)
bounds = seq(mi, ma, by = (ma-mi)/34)
bincenters = bounds[-1] - (ma-mi)/68 
CI
```
Now the actual plotting using the bounds specified above for the bins,
and the bincenters to plot the error bars
```{r}
hist(refractive, breaks = bounds, ylim = c(0,50)) 
arrows(x0 = bincenters, y0 = CI[1,], x1 = bincenters, y1 = CI[2,], 
	angle = 90, length = 0.05, code = 3)
axis(side = 1, at = seq(-7, 16, by = 1), labels = T)
```

Now I turn to Kernel Density Estimation; I'll use the Epanechnicov kernel. First let's define some functions
```{r}
epanechnikov = function(x) if ( abs(x) < sqrt(5)) {0.75 * (1-(x^2)/5)/sqrt(5)} else {0}
ep_kern_est = function(data, h, x){ 
	n = length(data)
	fn = 1/length(data) * sum( (1/h) * sapply((x-data)/h, epanechnikov))
	return(fn)
}
```
Let's test these functions out; To plot using curve, I need to Vectorize the estimator function 
so that given a vector input it returns the function applied to each input.
vectorize.args specifies which inputs should be vectorized.
```{r}
f <- Vectorize(ep_kern_est, vectorize.args =  c('x'))
#Non-vectorized function applied to integers from 1 to 10
ep_kern_est(refractive, 1, 1:10)
#Vectorized function applied to integers from 1 to 10
f(refractive, 1, 1:10)
```
Now I'm ready to test the function out 
I'll experiment with a few different bandwidths
```{r}
curve(f(refractive, 1, x), from = -10, to = 20)
curve(f(refractive, 0.2, x), from = -10, to = 20)
```
Now the difficult task is optimizing H, i.e., minimizing the risk
```{r}
g = Vectorize(epanechnikov)
J = function(h, data){
	n = length(data)
	g = Vectorize(epanechnikov)
	r = function(z, x) g(z-x) * g(x)
	K = function(y) integrate(r, -5, 5, z = y)$value  
	score = 2/(n*h) * epanechnikov(0)
	t  = 0
	for (i in 1:n){
		for (j in 1:n){
			y = (data[i] - data[j])/h
			num = K(y) - 2 *epanechnikov(y)
			t = t + num
			}
		}
	score = score + 1/(h*n^2) *t
	return (score)
}
J(1, refractive)
j_hat_val = seq(0.01, 1, length.out = 40)
min(sapply(j_hat_val, J, data = refractive))
#optimize(J, interval = c(.1, 1), data = refractive) commented out because very slow
J(0.2748, refractive)
```
So we get an (approximated) minimum of the cross-validation score at ~0.1722318.  
The kernel density estimator at this value looks like this:
```{r}
curve(f(refractive, 0.1722318, x), from = -10, to = 20, xlab = "refractive index", 
	ylab = "estimated density", col = "red", ylim = c(0, 0.4))
curve(f(refractive, 0.3, x), add = TRUE, col = "blue", lty = "dashed")
curve(f(refractive, 0.05, x), add = TRUE, col = "purple", lty = "dotted", 
	from = -7, to = 4)
curve(f(refractive, 0.05, x), add = TRUE, col = "purple", lty = "dotted", 
	from = 4, to = 20)
legend(5, 0.3, legend = c("0.1722 minimum CV score)", 
			"0.3 (oversmoothed)", 
			"0.05 (undersmoothed)"), 
			title = "Bandwidths",
			col = c("red", "blue", "purple"), lty = c("solid", "dashed", "dotted"))
```
Note the undersmoothed curve was drawn twice because otherwise certain points were missed due to the plotting
having resolution too low (not plotting any points within sqrt(5)/0.05 leading to no representation of point in plot)  


Now, let's make confidence intervals
```{r}
kernel_CI = function(data, h, x, alpha = 0.05){
	n = length(data)
	a = min(data)
	b = max(data)
	Y = (1/h) * sapply((x-data)/h, epanechnikov)
	ss = (1/(n-1)) * sum( (Y - mean(Y))^2)
	se = sqrt(ss/n)
	m = (b-a)/10
	q = qnorm( (1+ (1-alpha)^(1/m))/2)
	return(q * se)
}
Vkernel_CI = Vectorize(kernel_CI, vectorize.args = c('x'))
lower = function (data, h, x, alpha = 0.05 ) max(
			ep_kern_est(data, h,  x) - kernel_CI(data, h, x, alpha),
			0)
VLower = Vectorize(lower, vectorize.args = c('x'))
upper = function (data, h, x, alpha = 0.05) f(data, h,  x) + kernel_CI(data, h, x, alpha)
VUpper = Vectorize(upper, vectorize.args = c('x'))
```
And plotting 95% confidence interval with our optimized bandwidth
```{r}
h.hat = 0.1722318
curve(f(refractive, h.hat, x), from = -10, to = 20, xlab = "refractive index", 
	ylab = "estimated density",  ylim = c(0, 0.4))
xx = seq(from = -10, to = 20, length.out =2000)
polygon(c(xx, rev(xx)), c(
	VLower(refractive, h.hat, xx), 
	rev(VUpper(refractive, h.hat, xx))
	), 
	col = rgb(0.5, 0.5, 0.5 , 0.2), lty = "dotted")
```
Note that the confidence bands are clearly for the smoothed curves: their shape conforms to that of
our smoothed curve, so choosing different bandwidths gives different CIs. Thus the band really
represents a 95% confidence that the density we would produce for the chosen bandwidth
would fall within the given interval.   

Before I move on, its important to note that I made these functions just for practice;
the built-in density function can perform  kernel density estimation much more quickly:}
```{r}
plot(density(refractive, bw = "ucv", kernel = "epanechnikov"))

```
Note that this chose a bandwidth higher than the one chosen by optimizing my own function J.
That my function J wasn't too great is unsurprising; it relied on approximation of n integrals,
so likely had a lot of approximation error built up. Also it ran very slowly. A better implementation
would probably use the fast Fourier transform but I don't understand that well enough
to feel comfortable with implementing it yet.


#### **Exercise 3**

Consider the data from question 2. Let Y be the refractive index and let x be aluminum content
Perform a non-parametric regression to fit the model Y = f(x) + epsilon. Use cross-validation to estimate the bandwidth.
Construct 95% confidence intervals for your estimate.  
Let's plot the data in a scatter plot and make some functions using the Nadaraya-Watson estimator  
I'll again use the epanechnikov kernel because I already did the work for it;  
The __Nadaraya-Watson kernel estimator__ is defined by:  
\[\hat{r}(x) = \sum_{i=1}^{n} w_{i}(x)Y_i\]  
where
\[w_{i}(x) = \frac{K( \frac{x-x_{i}}{h})} {\sum_{j=1}^{n} K(\frac{x-x_{j}}{h})} \]
```{r}
Al = glass[["Al"]]
plot(x = Al, y = refractive, xlab = 'Aluminum Content', ylab = 'Refractive Index')
nad_Wats = function(x, xdata, ydata, h){
	#given an x-value and data on corresponding x and y values, produces the
	#Nadaraya-Watson estimator of Y = r(x) for bandwidth h
	k = sapply( (x-xdata)/h, epanechnikov)
	w = k/sum(k)
	est_y = sum ( w * ydata)
	return(est_y)
}
```
Let's plot this for a few different bandwidths
```{r}
Vnad_Wats = Vectorize(nad_Wats, vectorize.args = c("x"))
plot(x = Al, y = refractive, xlab = 'Aluminum Content', ylab = 'Refractive Index')
curve(Vnad_Wats(x, xdata = Al, ydata = refractive, h = 1), col = 1, add = TRUE)
curve(Vnad_Wats(x, xdata = Al, ydata = refractive, h = 0.5), col = 2, add = TRUE)
curve(Vnad_Wats(x, xdata = Al, ydata = refractive, h = 0.1), col = 3, add = TRUE)
legend(3, 10, title = "Bandwidths", col = c(1:3),lty = rep("solid", 3), 
	legend = c("1", "0.5", "0.1") )

```
As expected, lower bandwidths lead to a higher variance fit which follows the data more closely,
while higher bandwidths lead to a smoother curve.  


Now let's try to estimate the optimal bandwidth
The equation for estimating the risk is:
\[\hat{J}(h) = \sum{i=1}^{n} (Y_{i} - \hat{r}(x_{i}))^2 \frac {1} {(1 - \frac {K(0)}{\sum_{j=1}^{n} K(\frac{x_{i}-x_{j}}{h})})^2} \]
```{r, warning = FALSE}
NW_J_est = function(h, xdata, ydata){
	#given data on corresponding x and y-values, 
	#estimates risk function associated with bandwidth h
	est_y = sapply(xdata, nad_Wats, xdata = xdata, ydata = ydata, h = h)
	z = epanechnikov(0)
	k = function(x) 1/(1 -  z/sum( sapply( (x-xdata)/h, epanechnikov)))^2
	error = sum( (ydata - est_y)^2 * sapply(xdata, k) )
	return (error)
}



fit = optimize(NW_J_est, c(0.05, 1), xdata = Al, ydata = refractive) 
```
Now plotting the estimated cross-validation score as a function of h
```{r}
vNW_J_est = Vectorize(NW_J_est, vectorize.args = c("h"))
curve(vNW_J_est(h, Al, refractive), xlim = c(0.01, 1), xname = "h")
```
The properties of the epanechnikov kernel make this a bit odd: for values below the h chosen by
cross-validation, the score is actually infinity. This occurs because below that point, the largest
x-value, 3.5, is far enough from other points that the distance between it and any other point divided by h
is greater than sqrt(5). Thus, the nested sum only is only evaluated at xi = xj, which gives K(0), and so we
end up with $\frac {1} {(1 - \frac {K(0)}{K(0)})^2} = \frac{1}{0}$   
So we obtain infinite risk below the bandwidth which gives a minimal risk


Let's plot the estimate with the optimized bandwidth
```{r}
plot(x = Al, y = refractive, xlab = 'Aluminum Content', ylab = 'Refractive Index')
curve(Vnad_Wats(x, Al, refractive, h = fit$minimum), add = TRUE)
```
Now estimating confidence intervals. Similar to KDE, but our value for the standard error is different
```{r}
NW_CI = function(x, xdata, ydata, h, alpha = 0.05){
	#Confidence intervals for the Nadaraya-Watson estimator
	g = function(x, xdata) {
		k = sapply( (x-xdata)/h, epanechnikov)
		return (k/sum(k))
		}
	w = sapply(x, g, xdata)
	ydiff = diff(sort(ydata))
	n  = length(xdata)

	var = 1/( 2*(n-1)) * sum(ydiff^2)
	se = sqrt(var) * sqrt(colSums(w^2))

	m = (max(xdata) - min(xdata))/(10*h)

	q = qnorm((1 + (1-alpha)^(1/m))/2)

	rx = Vnad_Wats(x, xdata, ydata, h)
	upper = rx + q*se
	lower = rx - q*se
	return (cbind(lower, upper))
}

xx = seq(from = min(Al), to = max(Al), length.out = 2000)
plot(x = Al, y = refractive, xlab = 'Aluminum Content', ylab = 'Refractive Index')
curve(Vnad_Wats(x, Al, refractive, h = fit$minimum), add = TRUE)
NW_CI(c(1, 2, 3), Al, refractive, h = fit$minimum)
np_CI = NW_CI(xx, Al, refractive, fit$minimum)
polygon(c(xx, rev(xx)), 
	c(np_CI[,1], rev(np_CI[,2])),
	col = rgb(0.5, 0.5, 0.5, 0.2), lty = "dotted")
```



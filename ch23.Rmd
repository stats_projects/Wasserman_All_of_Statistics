---
title: "All of Statistics Chapter 3: Stochastic Processes"
output: 
 pdf_document:
  extra_dependencies: ["amsmath", "bm"] 
---

#### Exercise 23.1
Let $X_0, X_1, \dots$ be a Markov chain with states {0,1,2} and transition
matrix
\[\bm P= \begin{bmatrix}
	0.1 & 0.9 & 0.2\\
	0.9 & 0.1 & 0.0\\
	0.1 & 0.8 & 0.1\\
       \end{bmatrix}\]
Assume that $\mu_0 = (0.3,0.4,0.3$. Find $P(X_0 = 0, X_1 = 1, X_2=2)$
and $P(X_0 = 0, X_1 = 1, X_2 = 1$.  
We have
\[P(X_0 = 0, X_1 = 1, X_2=2) = 0.3 * 0.2 *0.0 = 0\]
and
\[P(X_0 = 0, X_1 = 1, X_2 = 1) = 0.3 * 0.2 * 0.1 = 0.015\]


#### Exercise 23.2
Let $Y_1, Y_2,\dots$ be a sequence of iid observations such that $P(Y=0)=0.1$, 
$P(Y=1) = 0.3$, $P(Y=2) = 0.2$, $P(Y=3)=0.4$. Let $X_0 = 0$ and let
\[X_n = Max\{Y_1, \dots, Y_n\}.\]
Show that $X_0, X_1, \dots$ is a Markov chain and find the transition matrix.  
The value of $X_n$ depends on $X_{n-1}$ through the fact that if 
$X_{n-1} = i$, then $X_n\ge i$. Knowing $X_{n-1}$ gives us all the relevant
information for determining $X_{n}$ from the first $n-1$ $Y$ values.
Formally, 
\begin{align}
	P(X_n = i | X_0, \dots, X_{n-1}) &= P(X_n = i | Y_1, \dots, Y_{n-1})\\
					 &= P(X_n = i | max{Y_1, \dots, Y_{n-1}}\\
					 &= P(X_n = i | X_{n-1})
\end{align}
To find the transition matrix, note that given $X_{n-1}$, the probability
that $X_n = X_{n-1}$ is the probability that $Y_n \le Y_{n-1}$, and the
probability that $X_n = i > X_{n-1}$ is just the probability that $Y_n = i$.
This gives us the following transition matrix:
\[\bm P= \begin{bmatrix}
		0.1 & 0.3 & 0.2 & 0.4 \\
		0   & 0.4 & 0.2 & 0.4 \\
		0   & 0   & 0.6 & 0.4 \\
		0   &     & 0   & 1
	\end{bmatrix}
\]

#### Exercise 23.3
Consider a two-state Markov chain with states$\chi={1,2}$ and transition matrix
\[\bm P = \begin{bmatrix}
		1-a & a\\
		b   & 1-b
	  \end{bmatrix}
\]
where $0<a<1$ and $0<b<1$. Prove that
\[\lim_{n\to\infty} \bm P^n =
\begin{bmatrix}
\frac{b}{a+b} & \frac{a}{a+b}\\
\frac{b}{a+b} & \frac{a}{a+b}
\end{bmatrix}
\]
The conditions on $a$ and $b$ mean the Markov chain is irreducible and recurrent.
Thus this is finding the stationary distribution, i.e.,
solving the left eigenvector with eigenvalue 1 of P
$\pi \bm P = \pi$ and constraint $\pi_1+\pi_2 = 1$. This gives us the linear system:
\[\pi_1 = \pi_1(1-a) + \pi_2(b)\\
\pi_2 =\pi_1(a) + \pi_2(1-b)\\
\pi_1 + \pi_2 = 1\]
Substitution gives $\pi_1(1-a) + (1-\pi_1)b= \pi_1)$ which gives $\pi_1 = \frac{b}{a+b}$, and
substitution into the last condition gives $\pi_2 = \frac{a}{a+b}$.

#### Exercise 23.4
*Consider the chain from question 3 and set $a=.1$ and $b=.3$. Simulate the chain
Let*
\[\hat p_n(1) = \frac{1}{n}\sum_{i=1}^n I(X_i=1)
\hat p_n(2) = \frac{1}{n}\sum_{i=1}^n I(X_i=2)\]
*be the proportion of times the chain is in state 1 and state 2. Plot $\hat p_n(1)$
and $\hat p_n(2)$ versus $n$ and verify that they converge to the values predicted
from the answer in the previous question.*
```{r}
twomarkov = function(a, b, state, states = c(1,2)){
	rand = runif(1)
	if (state == states[1]){
		if (rand > a) return(states[1]) else return(states[2])}
	if (state == states[2]){
		if (rand > b) return(states[2]) else return(states[1])}
}
states = c(rbinom(1,1,0.5)+1, rep(0,10000))
p1Hat = c(sum(states == 1), rep(0, 9999))
p2Hat = c(sum(states == 2), rep(0, 9999))
for (i in 2:10000){
	states[i] = twomarkov(0.1, 0.3, states[i-1])
	p1Hat[i] = sum(states == 1)/i
	p2Hat[i] = sum(states == 2)/i
	}
n = 1:10000
plot(n, p1Hat, ylim=c(0,1))
tail(p1Hat)
abline(h=0.3/(0.1+0.3))
plot(n, p2Hat, ylim=c(0,1)) 
abline(h=0.1/(0.1+0.3))
```

#### Exercise 23.5
An important Markov chain is the **branching process** which is used in biology, genetics,
nuclear physics, and many other fields. Suppose that an animal has Y children.
Let $p_k =P(Y = k)$.Hence, $p_k \ge 0$ for all $k$ and $\sum_{k=0}^\infty p_k=1$.
Assume that each animal has the same lifespan and that they produce offspring
according to the distribution $p_k$. Let $X_n$ be the number of animals in the
$n^{th}$ generation. Let $Y_1^{(n)},\dots,Y_{(X_n)}^{(n)}$ be the offspring
produced in the $n^{th}$ generation. Note that
\[X_{n+1} = Y_1^{(n)}+\dots+Y_{(X_n)}^{(n)}.\]
Let $\mu = E[Y]$ and $\sigma^2= Var(Y)$. Assume throughout this question that
$X_0 = 1$. Let $M(n)=E[X_n]$ and $V(n) = Var(X_n)$.

(a) Show that $M(n+1) = \mu M(n)$ and $V(n+1)= \sigma^2M(n)+\mu^2V(n)$.  
Using the law of iterated expectations, we have:
\[M(n+1) = E[X_{n+1}] = E[E[X_{n+1}|X_n]]= E[E[\sum_{i=1}^{X_n}Y_i^{(n)}| X_n]]
				= E[X_n E[Y]]
				= \mu E[X_n] = \mu M(n)\]
For the variance, first, we have using the law of total variance,
\begin{align}
V(n+1) = Var(X_{n+1}) &= E[Var(X_{n+1}|X_n) + Var(E[X_{n+1}|X_n)\\
		      &= E[Var(\sum_{i=1}^{X_n}Y_i^{(n)}| X_n)] 
		      	+ Var(E[\sum_{i=1}^{X_n}Y_i^{(n)}| X_n])\\
		      &= E[X_n Var(Y)] + Var(X_n E[Y])\\
		      &= \sigma^2 E[X_n] + \mu^2 Var(X_n) = \sigma^2M(n) + \mu^2V(n)
\end{align}

(b) Show that $M(n) = \mu M(n)$ and that $V(n) = \sigma^2 \mu^{n-1}(1+\mu+\dots+\mu^{n-1})$.  
Since we assume $X_0=1$, we have that $M(0) = E[X_0] = 1$, and by induction
using $M(n) = \mu M(n-1)$, $M(n) = \mu^n$ follows.  
For the variance, we can write
\begin{align}
V(n) &= \sigma^2 M(n-1) + \mu^2 V(n-1)\\
     &= \sigma^2 \mu^{n-1} + \mu^2(\sigma^2 \mu^{n-2} + \mu^2 V(n-2))\\
     &= \sigma^2 \mu^{n-1} (1 + \mu) + \mu^4 V(n-2) \\
     &= \sigma^2 \mu^{n-1} (1 + \mu) + \mu^4 (\sigma^2 \mu^{n-3} + \mu^2 V(n-3))\\
     &= \sigma^2 \mu^{n-1} (1 + \mu +\mu^2) + \mu^6 V(n-3))\\
     &=\dots
\end{align}
Continuing like this, we see we can write, for integers $k<n$,
\[V(n) = \sigma^2 \mu^{n-1}(1 + \mu +\dots+ \mu^{n-k-1}) + \mu^{2k} V(n - k).\]
Since $X_0$ is known, $V(0) = 0$, and we obtain the desired expression with $k=0$.  
(c) What happens to the variance if $\mu >1$? What happens to the variance if $\mu = 1$?
What happens to the variance if $\mu < 1$?  
If $\mu = 1$, $V(n)$ increases linearly with $n$ by the exact formula $V(n) = n\sigma^2$.
In the other two cases, we can write $V(n)$ explicitly using a geometric sum:
\[V(n) = \sigma^2 \mu^{n-1} \sum_{i=0}^{n-1} \mu^i = \sigma^2 \mu^{n-1}\frac{1-\mu^{n}}{1-\mu}
	= \sigma^2 \frac{\mu^{n-1} - \mu^{2n-1}}{1-\mu}\]
If $\mu > 1$, the numerator and thus the variance grow exponentially with n. 
If $\mu<1$, then the variance converges in the limit to 0.

(d) The population goes extinct if $X_n  = 0$ for some $n$. Let us thus define the extinction
time $N$ by
\[N = min\{ n:X_n = 0 \}.\]
Let $F(n) = P(N \le n)$ be the CDF of the random variable N. Show that
\[F(n) = \sum_{k=0}^{\infty}p_k(F(n-1))^k, \text{   } n=1,2,\dots\]
Note {$N \le n$} is the same as {$X_n = 0 | X_0 = 0$}, making our assumption on $X_0$ explicit 
We can thus write
\[F(n) = P(X_n = 0) = \sum_{k=0}^{\infty}P(X_1=k)P(X_n = 0 | X_1 = k)
	  = \sum_{k=0}^{\infty}p_kP(X_n = 0 | X_1 = k)\]
Now, it is clear that $P(X_n = 0 | X_1 = k) = P(X_{n-1} = 0 | X_0 = k)$. If the initial
population is $k$, then $X_{n-1}=0$ if and only if each of the $k$ lineages independently 
goes extinct in $n-1$ generations, and thus:
\[P(X_{n-1}=0|X_0 = k) = P(X_{n-1} = 0 | X_0 = 1)^k = (F(n-1))^k,\]
and
\[F(n) = \sum_{k=0}^{\infty} p_k (F(n-1))^k.\]  
\
(e) Suppose that $p_0 = 1/4$, $p_1=1/2$, $p_2 = 1/4$. Use the formula from part (5d)
to compute the CDF $F(n)$.  
We have $F(0) = 0$ and $F(1) = p_0 = 1/4$, $F(2) = 1/4 + (1/2)(1/4) + (1/4)^3$
For $F(n)$, we have
\[F(n) = 1/4 + 1/2F(n-1) +  1/4(F(n-1))^2 = 1/4 + 1/2(1/4 +1/2\]
```{r}
extinctionCDF = function(p, m){
	Fvec = rep(0, m+1)
	for (n in 2:(m+1)){
		Fn = 0
		for (k in 1:length(p)){
			Fn = Fn + p[k] * Fvec[n-1]^(k-1)
			}
		Fvec[n] = Fn
		}
	return (Fvec)
}	
pvec = c(1/4, 1/2,1/4)
F1000 = extinctionCDF(pvec, 1000)
plot(0:1000, F1000, xlab ="n", ylab = "F(n)", main = "Extinction CDF") 
```

#### Exercise 23.6
Let
\[ \bm P = \begin{bmatrix}
		0.40 & 0.50 & 0.10\\
		0.05 & 0.70 & 0.25\\
		0.05 & 0.50 & 0.45
	 \end{bmatrix}\]
Find the stationary distribution $\pi$.  
It is clear from inspection that this is an ergodic chain. So, 
just finding the 1 eigenvector of $(\bm P)^T$.
```{r}
P = cbind(c(0.40,0.05, 0.05), c(0.50, 0.70, 0.50), c(0.10,0.25, 0.45))
eigenP = eigen(t(P))
eigenP$values
```
The first eigenvalue is one.
```{r}
#picking out the 1-eigenvector
pi_1 = eigenP$vectors[,1]
#normalizing to probability distribution
pi_1 = pi_1/sum(pi_1)
pi_1
t(pi_1) %*% P - t(pi_1)
```
Above we have the stationary distribution, $\pi$ and $\pi \bm P - \pi$.
We see the numerical approximation is correct to 16 orders of magnitude.

#### Exercise 23.7
Show that if $i$ is a recurrent state and $i \leftrightarrow j$, then
$j$ is a recurrent state.  
\
Recall state $i$ is recurrent if 
\[P(X_n = i \text{ for some } n\ge 1|X_0 = i) =1\]
or equivalently, by Theorem 23.15, if
\[\sum_n p_{ii}(n) = \infty.\]
Now, $i \leftrightarrow j$ implies that we have positive integers
$m$ and $r$ such that $p_{ij}(m) > 0$ and $p_{ji}(r)>0$. By the Chapman-Kolgomorov
equations, we have for any natural number $N > r + m$,
\begin{align}
	p_{jj}(N) &= \sum_{k} p_{jk}(r)p_{kj}(N-r)\\
		  &= \sum_{k} p_{jk}(r)\sum_{s}p_{ks}(N-r-m)p_{sj}(m)\\
		  &\ge p_{ji}(r) p_{ii}(N-r-m) p_{ij}(m)
\end{align}
Thus, we have
\begin{align}
\sum_{n=0}^{\infty} p_{jj}(n) &\ge \sum_{n=r+m+1}^{\infty} p_{jj}(n)\\
		    	      & \ge \sum_{n=r+m+1}^{\infty} p_{ji}(r)p_{ii}(n-r-m)p_{ij}(m)\\
		     	      &= p_{ji}(r)p_{ij}(m) \sum_{n=r+m+1}^{\infty} p_{ii}(n-r-m)\\
		     	      &= \infty
\end{align}
So $\sum_{n=0}^{\infty} p_{jj}(n)=\infty$ and state $j$ must be recurrent.

#### Exercise 23.8
Let
\[\bm P = \begin{bmatrix}
	\frac{1}{3} & 0 	 & \frac{1}{3} & 0 & 0 & \frac{1}{3}  \\
	\frac{1}{2} & \frac{1}{4}& \frac{1}{4} & 0 & 0 & 0\\  
	0 	    &	       0 &	     0 & 0 & 1 & 0\\
	\frac{1}{4} & \frac{1}{4}& \frac{1}{4} & 0 & 0 & \frac{1}{4} \\
	0           & 0          & 1           & 0 & 0 & 0 \\
	0           & 0          & 0           & 0 & 0 & 1 
	\end{bmatrix}
\]
Which states are transient? Which states are recurrent?  
State $4$ is clearly transient, since the 4th column is all $0$s.
States $3$ and $5$ form a closed, irreducible set, and state
6 forms a closed irreducible set on its own. Then, note $1 \rightarrow 3$,
$2 \rightarrow 3$, so $1$ and $2$ cannot be in closed sets. Thus,
by the decomposition theorem, $1, 2, \text{ and } 4$ are transient
while $3, 5 \text{ and } 6$ are recurrent.

#### Exercise 23.9
Let 
\[ \bm P = \begin{bmatrix}
	0 & 1\\
	1 & 0
	\end{bmatrix}
\]
Show that $\pi = (1/2, 1/2)$ is a stationary distribution. Does this chain converge?
Why/why not?  
We have
\[\pi \bm P = (0*(1/2) + (1/2)*1, (1/2)*1 + 0*(1/2)) = (1/2, 1/2) = \pi.\]
However, this chain does not converge, since it is periodic, since we have
$p_{ii}(n) = 0$ whenever $n$ is not divisible by 2 for both states, and
thus $d(i) = 2 > 1$.





#### Exercise 23.10:
*Let the markov matrix for a discrete markov chain be*
\[ \bm P = \begin{bmatrix}
		q & q & q & q & 1\\
		p & 0 & 0 & 0 & 0\\
		0 & p & 0 & 0 & 0\\
		0 & 0 & p & 0 & 0\\
		0 & 0 & 0 & p & 0
	    \end{bmatrix}
	    \]
*with $0<p<1$ and $q = 1 - p$. Find the limiting distribution of the chain.*

As we know a limiting distribution exists, we know by Theorem 23.25 that 
the limiting distribution $\pi$ is the eigenvector of P with eigenvalue one.
I've written $P$ above as the transpose of $P$ from the text since I am more used
to working with column vectors and right multiplication.\
This gives us $P \pi = \pi$, or $(P - I) \pi = 0$. From the bottom 4 rows of
this linear system and the fact that $\pi$ is a distribution, we have the following:
\begin{gather} 
	\pi_{i+1} = \pi_i, i = 1, 2, 3, 4 \\
	\pi_1 + \pi_2 + \pi_3 + \pi_4 + \pi_5 = 1
\end{gather}
Solving this system gives us:
\[ \pi_1 = \frac{1}{1+p^2+p^3+p^4}\]
and
\[\pi = \frac{1}{1+p^2+p^3+p^4}\begin{bmatrix}
			   1 \\
			   p \\
			   p^2\\
			   p^3\\
			   p^4\\
			 \end{bmatrix}
\]

#### Exercise 23.11
Let $X(t)$ be an inhomogeneous Poisson process with intensity function $\lambda(t) > 0$.
Let $\Lambda (t) = \int_0^t \lambda(u) du$. Define $Y(s) = X(t)$, where $s = \Lambda(t)$.
Show that $Y(s)$ is a homogeneous Poisson process with intensity $\lambda = 1$.  
\
$Y(s)=X(t)$ immediately implies that $Y(s)$ fulfills the definition of a Poisson
process inherited from $X(t)$, so the only work is in determining the intensity function
$\lambda_Y(t)$. Denote the intensity function of $X(t)$ by $\lambda_X(t)$.
From Theorem 23.33, we have that $X(t)\sim Poisson(m_X(t))$ and $Y(s)\sim Poisson(m_Y(s))$,
where $m_X(t) = \int_0^t \lambda_X(u)du$ and similarly for $m_Y(t)$. Since 
$Y(s) = X(t)$, we have $m_X(t) = m_Y(s)$, which gives
\[\int_0^s \lambda_Y(u)du = \int_0^t \lambda_X(v)dv = \Lambda(t) = s\]
Thus, $Y(s) \sim Poisson(s)$, which implies $\lambda_Y(s) = 1$ and $Y(s)$ is a homogeneous
Poisson process.

#### Exercise 23.12  
Let X(t) be a Poisson process with intensity $\lambda$. Find the conditional distribution
of $X(t)$ given that $X(t+s) = n$.  
\
Note that $X(t) \sim$ Poisson$(\lambda t)$, $X(s+t) - X(t)\sim$ Poisson$(\lambda s)$, and
$X(t)$ and $X(s+t) - X(t)$ are independent. Then we have
\[
\begin{aligned}
P(X(t) = x | X(s+t) = n) &= \frac{P(X(t) = x \text{ and } X(s+t) = n)}{P(X(s+t) = n)}\\
			 &= \frac{P(X(t) = x \text{ and } X(s+t)-X(t) = n-x)}{P(X(s+t) = n)}\\
			 &= \frac{e^{-\lambda t}{(\lambda t)}^x e^{-\lambda s}{(\lambda s)}^{n-x} n!}
			 	{x!(n-x)! e^{ -\lambda (s + t)}{(\lambda (s+t))}^n}\\
			 &= \frac{n!\lambda^n t^x  s^{n-x}}
			 	{x!(n-x)! \lambda^n({(s+t)}^x{(s+t)}^{n-x})}\\
			 &= \binom{n}{x} {\Bigl(\frac{t}{s+t}\Bigr)}^x 
			 	{\Bigl(\frac{s}{{s+t}}\Bigr)}^{n-x},
\end{aligned}\]
for $0\le x\le n$, i.e, $(X(t)|X(s+t) = n) \sim$ Binomial$(n,\frac{t}{s+t})$.

#### Exercise 23.13  
Let $X(t)$ be a Poisson process with intensity $\lambda$. Find the probability
that $X(t)$ is odd.  
\
Note $\lambda >0$, and $X(t)$ has PMF
\[P(X(t) = x) = \frac{e^{-\lambda t}{(\lambda t)}^x}{x!},\ x=0,1,2,\dots\]
The probability that $X(t)$ is odd can be expressed as:
\[P(X(t) = 1, 3, 5, \dots) = \frac{1}{2}(\sum_{n=0}^{\infty}\frac{e^{-\lambda t}{(\lambda t)}^x}{x!}-
	\sum_{n=0}^{\infty}\frac{e^{-\lambda t}{(-\lambda t)}^x}{x!})\]
In effect, we sum over the PMF, and subtract the even terms. While subtracting the
even terms, we add the odd terms a second time so we multiply by $\frac{1}{2}$.We can then
pull out the exponential, leaving us with
\[P(X(t) = 1, 3, 5, \dots) =\frac{e^{-\lambda t}}{2}(\sum_{n=0}^{\infty}\frac{{(\lambda t)}^x}{x!}-
	\sum_{n=0}^{\infty}\frac{{(-\lambda t)}^x}{x!})
	=\frac{e^{-\lambda t}}{2}(e^{\lambda t} - e^{-\lambda t})
	=\frac{1-e^{-2\lambda t}}{2}\]


#### Exercise 21.16
A two-dimensional Poisson point process is a process of random points on the plane
such that (i) for any set A, the number of points falling in A is Poisson with mean 
$\lambda \mu (A)$, (ii) the number of events in non-overlapping regions is independent.
Consider an arbitrary point $x_0$ in the plane. Let $X$ denote the distance from $x_0$
to the nearest random point. Show that
\[P(X > t) = e^{-\lambda \pi t^2}\]
and
\[E[X] = \frac{1}{2\sqrt{\lambda}}\]
\
Note the event $X>t$ occurs if and only if the circle of radius $t$ around $x_0$ contains
no points from the process. Let $Y(t)$ denote the number of points in this circle.
Then $Y(t)$ is Poisson with mean $\lambda \pi t^2$, and we have
\[P(X>t) = P(Y(t) = 0) = e^{-\lambda \pi t^2}.\]
The CDF of X is then $F(x) = P(X \le t) = 1- e^{- \lambda \pi t^2}$, and differentiating
we obtain the density function of X as
\[f(x) = 2 \lambda \pi t e^{-\lambda \pi t^2}.\]
Thus, 
\[E[X] = \int_0^{\infty} 2 \lambda \pi t^2 e^{-\lambda \pi t^2} dt.\]
With the substitutions $u = t^2$ and $du = 2t dt$ (so $dt = \frac{du}{2u^{1/2}}$)
and using the definition of a Gamma PDF, we have
\[\begin{aligned}
E[X] &= \int_0^{\infty} \lambda \pi u^{1/2} e^{-\lambda \pi u} du \\
     &=\frac{\Gamma (3/2)}{{(\lambda \pi})^{1/2}} 
     \int_0^{\infty} \frac{u^{1/2} {(\lambda \pi)}^{3/2}}{\Gamma (3/2)} e^{-\lambda \pi u} du\\
     &=\frac{\Gamma (3/2)}{{(\lambda \pi)}^{1/2}}\\ 
     &=\frac{\sqrt{\pi}/2}{{(\lambda \pi)}^{1/2}}\\
     &= \frac{1}{2 \sqrt{\lambda}}.
\end{aligned}\]
